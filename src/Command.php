<?php
namespace Swork;

use Swork\Bean\Scanner;
use Swork\Model\Generator;

class Command
{
    /**
     * 当前命令
     * @var array
     */
    public static $cmd;

    /**
     * 启动入口
     * @param array $env
     */
    public static function Run(array &$env)
    {
        //补充环境参数
        $env['env'] = '';
        $env['runtime'] = $env['root'] . 'runtime' . $env['sep'];

        //判断是否存在环境配置的不同版本
        $ini = $env['root'] . 'config.ini';
        if (file_exists($ini))
        {
            $cfg = parse_ini_file($ini, true);
            $env['env'] = $cfg['system']['env'] ?? '';
        }

        //初始化配置
        if (!Configer::collect($env))
        {
            return;
        }

        //初始化PID管理器
        Service::$pidManager = new PidManager($env);

        //运行命令
        list($cmd, $opt) = self::getArgs();
        self::$cmd = $cmd;
        switch ($cmd)
        {
            case 'start':
                self::start($env, $opt);
                break;
            case 'restart':
                self::restart($env, $opt);
                break;
            case 'stop':
                self::stop($opt);
                break;
            case 'model':
                self::model($env, $opt);
                break;
            case 'view':
                self::view($env, $opt);
                break;
            case 'doc':
                self::doc($env, $opt);
                break;
            case 'tasks':
                self::tasks($env, $opt);
                break;
            default:
                self::showTag('ERR', 'not exist command');
        }
    }

    private static function start(array $env, array $opt)
    {
        $service = new Service($env);
        $service->start($opt);
        self::showTag('Server', 'started');
        return true;
    }

    private static function restart(array $env, array $opt)
    {
        //关闭服务
        self::stop($opt);

        //启动服务
        $service = new Service($env);
        $service->start($opt);
        self::showTag('Server', 'restarted');
        return true;
    }

    private static function stop($opt)
    {
        //是否强制停止
        $force = isset($opt['f']);

        //文件中取出pid组
        $pidArr = Service::$pidManager->readPidArray();

        //等待停止成功
        $awaitStop = function ($pid)
        {
            while (true)
            {
                //如果停止了，断开循环
                if (!\swoole_process::kill($pid, 0))
                {
                    break;
                }
            }
        };

        //区分普通停止和强制停止
        if ($force)
        {
            foreach ($pidArr as $key => $item)
            {
                self::showTag('stop force', "Killed pid: $item");
                if (\swoole_process::kill($item, 0))
                {
                    \swoole_process::kill($item, 9);
                }

                //等待停止成功
                $awaitStop($item);
            }
            self::showTag('stop force', "Completed!");
        }
        else
        {
            $master_pid = $pidArr['master'] ?? 0;
            if ($master_pid <= 0 || !\swoole_process::kill($master_pid, 0))
            {
                self::showTag('stop', "PID :{$master_pid} not exist");
                return false;
            }
            \swoole_process::kill($master_pid);

            //等待停止成功
            $awaitStop($master_pid);
        }

        //返回
        return true;
    }

    /**
     * 执行生成Model
     * @param array $env 运行环境
     * @param array $opt 操作参数
     */
    private static function model(array $env, array $opt)
    {
        go(function () use ($env, $opt)
        {
            $gen = new Generator($env);
            $gen->create($opt);
            die();
        });
    }

    /**
     * 执行生成view
     * @param array $env 运行环境
     * @param array $opt 操作参数
     */
    private static function view(array $env, array $opt)
    {
        go(function () use ($env, $opt)
        {
            $scanner = new Scanner($env, 0);
            $scanner->collect();
            $scanner->getController();
            die();
        });
    }

    /**
     * 执行生成文档
     * @param array $env 运行环境
     * @param array $opt 操作参数
     */
    private static function doc(array $env, array $opt)
    {
        go(function () use ($env, $opt)
        {
            $scanner = new Scanner($env, 0);
            $scanner->collect();
            $scanner->getController();
        });
    }

    /**
     * 执行生成Task列表
     * @param array $env 运行环境
     * @param array $opt 操作参数
     */
    private static function tasks(array $env, array $opt)
    {
        go(function () use ($env, $opt)
        {
            $scanner = new Scanner($env);
            $scanner->collect();
            $tasks = $scanner->getTimerTask(false);

            //合并内容
            $tmp = [];
            foreach (($tasks['timer'] ?? []) as $tick => $items)
            {
                foreach ($items as $key => $item)
                {
                    $cls = $item['cls'];
                    $name = $item['name'];
                    $tmp[] = $cls . '::' . $name . ' => ' . $tick;
                }
            }
            sort($tmp);
            $txt = join("\r\n", $tmp);

            //写入文件
            $root = $env['root'];
            $sep = $env['sep'];

            //文件夹
            $path = $root . 'runtime' . $sep;
            if (!file_exists($path))
            {
                mkdir($path, 777, true);
            }

            //目标文件
            $fp = fopen($path . 'task.log', 'w+');
            fwrite($fp, $txt);
            fclose($fp);

            //页面输出
            foreach ($tmp as $value)
            {
                echo $value . "\r\n";
            }
        });
    }

    /**
     * 外部命令参数
     * @return array
     */
    private static function getArgs()
    {
        global $argv;
        $command = '';
        $options = array();

        //提取命令
        if (isset($argv[1]))
        {
            $command = strtolower($argv[1]);
        }

        //提取参数
        $args = strtolower(join(' ', $argv));
        if (preg_match_all('/[\-]+(\w+)\s?([^\-\s]+)?/', $args, $match))
        {
            foreach ($match[1] as $key => $item)
            {
                $options[$item] = trim($match[2][$key]);
            }
        }

        //返回
        return array($command, $options);
    }

    /**
     * 屏幕输出内容
     * @param string $name 输出的KEY
     * @param string $value 输出的内容
     */
    public static function showTag($name, $value)
    {
        echo "\e[32m" . str_pad($name, 20, ' ', STR_PAD_RIGHT) . "\e[34m" . $value . "\e[0m\n";
    }
}
