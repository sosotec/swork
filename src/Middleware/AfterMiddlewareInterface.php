<?php
namespace Swork\Middleware;

use Swork\Server\ArgumentInterface;

interface AfterMiddlewareInterface
{
    /**
     * 中间件处理层（请求后的处理）
     * @param ArgumentInterface $argument 请求参数
     * @param mixed $result 逻辑处理后的结果
     * @return bool
     */
    public function process(ArgumentInterface $argument, &$result);
}
