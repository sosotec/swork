<?php
namespace Swork\Middleware;

use Swork\Bean\BeanCollector;
use Swork\Helper\TcpHelper;
use Swork\Server\ArgumentInterface;

/**
 * 默认TCP发送消息中间件处理器
 * Class DefaultTcpPushMiddleware
 * @package Swork\Middleware
 */
class DefaultTcpPushMiddleware extends BeanCollector implements AfterMiddlewareInterface
{
    /**
     * 中间件处理层，按 {"cmd" => "content"} 的方式处理返回数据
     * @param ArgumentInterface $argument 请求参数
     * @param mixed $result 逻辑处理后的结果
     */
    public function process(ArgumentInterface $argument, &$result)
    {
        if (is_array($result))
        {
            foreach ($result as $key => $value)
            {
                $result = TcpHelper::assemblyContent($key, $value);
                return;
            }
        }
        if (!is_string($result))
        {
            $result = strval($result);
        }
        $result = TcpHelper::assemblyContent($result, '');
    }
}

