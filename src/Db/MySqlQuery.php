<?php
namespace Swork\Db;

use Swork\Exception\DbException;

class MySqlQuery
{
    /**
     * 支持的连接符
     */
    const CONNECTORS = [
        '$OR' => 1,
        '$AND' => 2,
        '$GROUP' => 3,
        '$INDEX' => 4,
        '$HAVING' => 5,
    ];

    /**
     * 连接符
     */
    const OP_AND = ' AND ';
    const OP_OR = ' OR ';

    /**
     * 外部参数
     * @var array
     */
    private $_tbl = null;
    private $_cols = null;
    private $_where = null;
    private $_order = null;

    /**
     * 字符储存容器
     * @var array
     */
    private $sql = [];
    private $values = [];
    private $types = [];

    /**
     * 当前操作连接（and, or）
     * @var string
     */
    private $currentOP = null;

    /**
     * force index 强制索引
     * @var string
     */
    private $index = null;

    /**
     * group by 字段
     * @var string
     */
    private $group = null;

    /**
     * having 字段
     * @var array
     */
    private $having = [];

    /**
     * 编译后的SQL缓存
     * @var array
     */
    private static $cache = [];

    /**
     * MySqlQuery constructor.
     * @param string $tbl 当前表名（或主表名）
     * @param array $cols 实例的字段定义对象
     * @param array $where 数据条件
     * @param array $order 排序字段
     */
    public function __construct(string $tbl, array $cols, array $where, array $order)
    {
        $this->_tbl = $tbl;
        $this->_cols = $cols;
        $this->_where = $where;
        $this->_order = $order;
    }

    /**
     * 获取SQL数据条件
     * @return array
     * @throws
     */
    public function getCondition()
    {
        /**
         * { id => 1 } : where id=1
         * { id => 1, name => xx } : where id=1 and name=xx
         * { id => 1, name => ['like' => '%a%'] } : where id=1 and name like '%a%'
         * { id => 1, age => {'>' => 10} } : where id=1 and age > 10
         * { id => 1, sex => {'in' => [1,2]} } : where id=1 and sex in (1,2)
         * { id => 1, age => {'>' => 10, '<' => 100} } : where id=1 and age > 10 and age < 100
         * { id => 1, '$or' => [ {age => {'<' => 10}}, {age => {'>' => 100} } ] } : where id=1 and (age < 10 or age > 100)
         * { id => 1, '$or' => [ {age => {'<' => 10}}, {'$and' => [{age => {'>' => 100}}, {sex => 1}] ] } : where id=1 and (age < 10 or (age > 100 and sex=1))
         * { id => 1, '$group' => [ age, sex ] } : where id=1 group by age,sex
         * { '$or' => [{ id => 1}, {name => xx }]} : where id=1 or name=xx
         * { '$or' => [{ id => 1}, {name => xx }], sex => 1} : where (id=1 or name=xx) and sex=1
         * { '$index' => 'idx1,idx2'} : select * from T FORCE INDEX (idx1,idx2) where ......
         * { '$index' => 'A.idx1,B.idx2'} : 在join表中实现，A和B表示第1第2张表
         * { '$having' => [id => 1]} : group by xxx having yyy
         * { ':col1' => 'col2'} : where col1 = col2 (两个字段相同)
         * { ':col2' => {'>' => 'col1'}} : where col1 > col2 (前字段大于后字段)
         * { ':col3' => 1} : where col1 = 1 (直接字段等于某值，不使用参数)
         */

        //先默认从缓存中获取
        $ckey = md5($this->_tbl . serialize($this->_where));
        if (isset(self::$cache[$ckey]))
        {
            return self::$cache[$ckey];
        }

        //如果不为空值
        if (!empty($this->_where))
        {
            $this->currentOP = self::OP_AND;
            $this->explain($this->_where);
        }

        //合并SQL
        $where = null;
        if (count($this->sql) > 0)
        {
            $where .= ' WHERE ' . join('', $this->sql);
        }
        if ($this->group != false)
        {
            $where .= ' GROUP BY ' . $this->group;
        }

        //如果有having条件则继续拼接SQL
        if (count($this->having) > 0)
        {
            $this->sql = [];
            $this->explain($this->having);
            if (count($this->sql) > 0)
            {
                $where .= ' HAVING ' . join('', $this->sql);
            }
        }

        //合成数值
        $value = [
            'where' => $where,
            'index' => $this->index,
            'types' => $this->types,
            'values' => $this->values,
        ];

        //写入缓存（当前线程有效，不是跨线程共享）
        self::$cache[$ckey] = $value;

        //返回
        return $value;
    }

    /**
     * 把？号替换至真实的数值
     * @return mixed|string
     */
    public function getTransform()
    {
        $condition = $this->getCondition();
        $where = $condition['where'];
        $types = $condition['types'];
        $values = $condition['values'];

        //如果没有？号
        if (count($this->types) == 0)
        {
            return $where;
        }

        //替换？号
        $sql = [];
        foreach (explode('?', $where) as $key => $item)
        {
            if ($key > 0)
            {
                $type = $types[$key - 1];
                if ($type == 'i' || $types == 'd')
                {
                    $sql[] = $values[$key - 1];
                }
                else
                {
                    $sql[] = '\'' . $values[$key - 1] . '\'';
                }
            }
            $sql[] = $item;
        }

        //合并返回
        return join('', $sql);
    }

    /**
     * 获取排序语句
     * @return null|string
     */
    public function getOrderBy()
    {
        if (empty($this->_order))
        {
            return null;
        }

        $orderby = [];
        foreach ($this->_order as $key => $value)
        {
            $orderby[] = $key . ' ' . ($value == 1 ? 'ASC' : 'DESC');
        }

        return join(',', $orderby);
    }

    /**
     * 解析数据条件
     * @param array $where
     * @throws
     */
    private function explain(array $where)
    {
        foreach ($where as $key => $val)
        {
            $key = trim($key);
            $idx = strtoupper($key);
            if (isset(self::CONNECTORS[$idx]))
            {
                switch ($idx)
                {
                    case '$OR':
                    case '$AND':
                        $tmpOP = $this->currentOP;
                        $this->pushCurrentOP();
                        $this->currentOP = ($idx == '$OR') ? self::OP_OR : self::OP_AND;
                        $this->sql[] = "(";

                        foreach ($val as $nm => $va)
                        {
                            $this->explain($va);
                        }

                        $this->sql[] = ")";
                        $this->currentOP = $tmpOP;
                        break;
                    case '$INDEX':
                        if (preg_match_all('/([A-H])\.([^,\s]+)/', $val, $matchs))
                        {
                            //分组索引
                            $temps = [];
                            foreach ($matchs[1] as $sid => $short)
                            {
                                $temps[$short][] = $matchs[2][$sid];
                            }

                            //重组索引
                            $indexs = [];
                            foreach ($temps as $short => $items)
                            {
                                $indexs[$short] = ' FORCE INDEX (' . join(',', $items) . ')';
                            }
                            $this->index = $indexs;
                        }
                        else
                        {
                            $this->index = " FORCE INDEX ($val)";
                        }
                        break;
                    case '$GROUP':
                        $this->group = is_array($val) ? join(',', $val) : $val;
                        break;
                    case '$HAVING':
                        $this->having = $val;
                        break;
                }
            }
            elseif (substr($key, 0, 1) == ':')
            {
                $key = substr($key, 1);
                if (is_array($val))
                {
                    if (count($val) == 0)
                    {
                        throw new DbException("The column [{$this->_tbl}.$key] does not have value.", 3030);
                    }
                    foreach ($val as $op => $va)
                    {
                        $op = trim($op);
                        $op = strtoupper($op);
                        switch ($op)
                        {
                            case '>':
                            case '>=':
                            case '<':
                            case '<=':
                            case '=':
                            case '!=':
                            case 'LIKE':
                            case 'NOT LIKE':
                                $this->pushCurrentOP();
                                $this->sql[] = "$key $op $va";
                                break;
                            case 'IN':
                            case 'NOT IN':
                                $tmp = $this->checkInOperateValue($key, $va);
                                $this->pushCurrentOP();
                                if (count($tmp) <= 1)
                                {
                                    $this->sql[] = $key . ($op == 'IN' ? "=$tmp[0]" : "!=$tmp[0]");
                                }
                                else
                                {
                                    $this->sql[] = $key . ' ' . $op . ' (' . join(',', $tmp) . ')';
                                }
                                break;
                            case 'BETWEEN':
                            case 'NOT BETWEEN':
                                $this->pushCurrentOP();
                                $this->sql[] = "$key $op $va[0] AND $va[1]";
                                break;
                            case '0':
                                $tmp = $this->checkInOperateValue($key, $val);
                                $this->pushCurrentOP();
                                if (count($tmp) <= 1)
                                {
                                    $this->sql[] = $key . ' = ' . $tmp[0];
                                }
                                else
                                {
                                    $this->sql[] = $key . ' IN (' . join(',', $tmp) . ')';
                                }
                                break 2;
                            default:
                                throw new DbException("The  column [{$this->_tbl}.$key] does not match operator.", 3031);
                                break;
                        }
                    }
                }
                else
                {
                    $this->pushCurrentOP();
                    $this->sql[] = "$key = $val";
                }
            }
            else
            {
                if (is_array($val))
                {
                    if (count($val) == 0)
                    {
                        throw new DbException("The column [{$this->_tbl}.$key] does not have value.", 3030);
                    }
                    foreach ($val as $op => $va)
                    {
                        $op = trim($op);
                        $op = strtoupper($op);
                        switch ($op)
                        {
                            case '>':
                            case '>=':
                            case '<':
                            case '<=':
                            case '=':
                            case '!=':
                            case 'LIKE':
                            case 'NOT LIKE':
                                $this->pushCurrentOP();
                                $this->sql[] = "$key $op ?";
                                $this->types[] = $this->getType($key);
                                $this->values[] = $va;
                                break;
                            case 'IN':
                            case 'NOT IN':
                                $tmp = $this->checkInOperateValue($key, $va);
                                $this->pushCurrentOP();
                                if (count($tmp) <= 1)
                                {
                                    $this->sql[] = $key . ($op == 'IN' ? '=?' : '!=?');
                                    $this->types[] = $this->getType($key);
                                    $this->values[] = $tmp[0];
                                }
                                else
                                {
                                    $marks = [];
                                    foreach ($tmp as $v)
                                    {
                                        $marks[] = '?';
                                        $this->types[] = $this->getType($key);
                                        $this->values[] = $v;
                                    }
                                    $marks = join(',', $marks);
                                    $this->sql[] = "$key $op ($marks)";
                                }
                                break;
                            case 'BETWEEN':
                            case 'NOT BETWEEN':
                                $this->pushCurrentOP();
                                $this->sql[] = "$key $op ? AND ?";
                                $this->types[] = $this->getType($key);
                                $this->types[] = $this->getType($key);
                                $this->values[] = $va[0];
                                $this->values[] = $va[1];
                                break;
                            case '0':
                                $tmp = $this->checkInOperateValue($key, $val);
                                $this->pushCurrentOP();
                                if (count($tmp) <= 1)
                                {
                                    $this->sql[] = "$key = ?";
                                    $this->types[] = $this->getType($key);
                                    $this->values[] = $tmp[0];
                                }
                                else
                                {
                                    $marks = [];
                                    foreach ($tmp as $v)
                                    {
                                        $marks[] = '?';
                                        $this->types[] = $this->getType($key);
                                        $this->values[] = $v;
                                    }
                                    $marks = join(',', $marks);
                                    $this->sql[] = "$key IN ($marks)";
                                }
                                break 2;
                            default:
                                throw new DbException("The column [{$this->_tbl}.$key] does not match operator.", 3031);
                                break;
                        }
                    }
                }
                else
                {
                    $this->pushCurrentOP();
                    $this->sql[] = "$key = ?";
                    $this->types[] = $this->getType($key);
                    $this->values[] = $val;
                }
            }
        }
    }

    /**
     * 获取字段类型
     * @param string $key 字段名称
     * @return string
     * @throws
     */
    private function getType(string $key)
    {
        if (!isset($this->_cols[$key]))
        {
            throw new DbException("The column [{$this->_tbl}.$key] does not exist.", 3033);
        }
        return $this->_cols[$key][0];
    }

    /**
     * 累加操作
     */
    private function pushCurrentOP()
    {
        if (count($this->sql) > 0 && end($this->sql) != '(')
        {
            $this->sql[] = $this->currentOP;
        }
    }

    /**
     * 检查IN操作符的合法数据结构
     * @param string $key 字段名
     * @param array $vals IN的数组
     * @return array
     * @throws
     */
    private function checkInOperateValue(string $key, array $vals)
    {
        $vals = array_values($vals);
        if (count($vals) == 0)
        {
            throw new DbException("The column [{$this->_tbl}.$key] does not have value.", 3030);
        }
        foreach ($vals as $val)
        {
            if (is_array($val))
            {
                throw new DbException("The column [{$this->_tbl}.$key] does not match value type.", 3032);
            }
        }
        return $vals;
    }
}
