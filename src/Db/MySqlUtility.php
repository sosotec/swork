<?php
namespace Swork\Db;

/**
 * MySQL的工具
 * Class MySqlUtility
 * @package Swork\Db
 */
class MySqlUtility
{
    /**
     * 强制转换至字段应有的数据类型
     * @param array $cols 所有字段集合
     * @param array $list
     */
    public static function forceToDataType(array $cols, &$list)
    {
        if ($list == false)
        {
            return;
        }
        foreach ($list as $key => $item)
        {
            if (is_int($key))
            {
                foreach ($item as $col => $val)
                {
                    $list[$key][$col] = self::convertToDataType($cols, $col, $val);
                }
            }
            else
            {
                $list[$key] = self::convertToDataType($cols, $key, $item);
            }
        }
    }

    /**
     * 根据字义的数据类型转换成相应的数据类型
     * @param array $cols 所有字段集合
     * @param string $col 字段名
     * @param mixed $val 字段值
     * @return mixed
     */
    public static function convertToDataType(array $cols, string $col, $val)
    {
        $type = $cols[$col][0] ?? false;
        if ($type == false)
        {
            return $val;
        }
        if ($type == 'i')
        {
            return intval($val);
        }
        if ($type == 'd')
        {
            return floatval($val);
        }
        return $val;
    }
}
