<?php
namespace Swork\Server;

/**
 * 定时器
 */
class Timer
{
    /**
     * @var \swoole_websocket_server
     */
    private $server;

    /**
     * Timer constructor.
     * @param \swoole_websocket_server $server
     */
    public function __construct(\swoole_websocket_server $server)
    {
        $this->server = $server;
    }

    /**
     * 执行定时任务
     * @param array $tasks
     */
    function tickTask(array $tasks)
    {
        foreach (($tasks['timer'] ?? []) as $time => $items)
        {
            $this->server->tick($time, function () use ($items)
            {
                foreach ($items as $item)
                {
                    $info = [
                        'act' => 'TimerTask',
                        'cls' => $item['cls'],
                        'name' => $item['name'],
                    ];
                    $this->server->task(serialize($info));
                }
            });
        }
    }
}
