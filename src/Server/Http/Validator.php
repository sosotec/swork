<?php
namespace Swork\Server\Http;

use Swork\Bean\Holder\ValidateHolder;
use Swork\Exception\ValidateException;

/**
 * Http验证处理器
 * Class Validator
 * @package Swork\Server\Http
 */
class Validator
{
    /**
     * 检查
     * @param string $cls 请求类
     * @param string $method 请求执行的方法
     * @param Argument $argument 传入参数
     * @throws
     */
    public static function check(string $cls, string $method, Argument $argument)
    {
        //提取当前类的检验器和方式
        $validate = ValidateHolder::getClass($cls)[$method] ?? false;
        if ($validate == false)
        {
            return;
        }

        //检查请求类型
        $method = $validate['method'] ?? 'GET';
        if ($method != $argument->swoole_request->server['request_method'])
        {
            throw new ValidateException("当前接口要求[$method]方式", 999);
        }

        //提取规则，并检查
        $rule = $validate['rule'] ?? [];
        if (count($rule) == 0)
        {
            return;
        }

        //根据每个参数处理
        foreach ($rule as $name => $calls)
        {
            //获取数据值
            $val = null;
            if ($method == 'GET')
            {
                $val = $argument->get($name);
            }
            else
            {
                $val = $argument->post($name);
            }

            //检验当前字段这下所有规则
            foreach ($calls as $call)
            {
                $target = $call['target'];
                $void = $call['void'];
                if (in_array($void, ['Equal', 'Greater', 'GreaterEqual', 'Lesser', 'lesserEqual']))
                {
                    if (isset($call['match'][0]) && $call['match'][0] == '')
                    {
                        $call['val2'] = $call['match'][1] ?? '';
                    }
                    else
                    {
                        $name2 = $call['match'][1] ?? '';
                        $call['val2'] = $method == 'GET' ? $argument->get($name2) : $argument->post($name2);
                    }
                }
                $void = lcfirst($void);
                $target::$void($name, $val, $call);
            }
        }
    }
}
