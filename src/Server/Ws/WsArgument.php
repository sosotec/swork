<?php
namespace Swork\Server\Ws;

use Swork\Helper\DataHelper;
use Swork\Server\ArgumentInterface;

/**
 * WebSocket传输的参数对象
 * Class WsArgument
 * @package Swork\Server\WebSocket
 */
class WsArgument implements ArgumentInterface
{
    /**
     * server
     * @var \swoole_websocket_server
     */
    private $server;

    /**
     * 来源fd
     * @var int
     */
    private $fd;

    /**
     * 请求URI
     * @var string
     */
    private $uri;

    /**
     * 用户URL请求参数
     * @var array
     */
    private $user_gets = [];

    /**
     * 内容实体
     * @var mixed
     */
    private $body;

    /**
     * 用户简易路径的参数
     * @var array
     */
    private $params = [];

    /**
     * websocket消息体
     * @var \swoole_websocket_frame
     */
    private $frame = null;

    /**
     * 是否停止往下运行
     * @var bool
     */
    private $break = false;

    /**
     * WsArgument constructor.
     * @param \swoole_websocket_server $server
     * @param int $fd
     * @param string $uri 请求地址
     */
    public function __construct(\swoole_websocket_server $server, int $fd, string $uri)
    {
        $this->server = $server;
        $this->fd = $fd;
        $this->uri = $uri;
    }

    /**
     * @return \swoole_websocket_server
     */
    public function getServer(): \swoole_websocket_server
    {
        return $this->server;
    }

    /**
     * @param \swoole_websocket_frame $frame
     */
    public function setFrame(\swoole_websocket_frame $frame)
    {
        $this->frame = $frame;
    }

    /**
     * @return \swoole_websocket_frame
     */
    public function getFrame(): \swoole_websocket_frame
    {
        return $this->frame;
    }

    /**
     * 设置返回的Header
     * @param string $key 头KEY
     * @param string $value 具体内容
     */
    public function setHeader(string $key, string $value)
    {

    }

    /**
     * 设置截断
     */
    public function setBreak()
    {
        $this->break = true;
    }

    /**
     * 判断是否截断
     */
    public function isBreak()
    {
        return $this->break;
    }

    /**
     * 设置用户请求的数据
     * @param array $gets
     */
    public function setUserGet(array $gets)
    {
        $this->user_gets = $gets;
    }

    /**
     * 获取连接句柄
     * @return int
     */
    public function getFd()
    {
        return $this->fd;
    }

    /**
     * 设置路由地址
     * @param string $uri
     */
    public function setUri(string $uri)
    {
        $this->uri = $uri;
    }

    /**
     * 获取路由地址
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * 设置内容
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * 获取内容
     * @return mixed 内容实体
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * 设置参数
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * 获取客户端IP
     * @return string
     */
    public function getUserIP()
    {
        return $this->server->getClientInfo($this->fd)['remote_ip'] ?? '';
    }

    /**
     * 根据key取出内容值（在Open时有效）
     * @param string|null $key 键
     * @param mixed $dft 默认值
     * @return mixed
     */
    public function get($key = null, $dft = null)
    {
        if ($key == null)
        {
            return $this->user_gets;
        }
        if (isset($this->user_gets[$key]))
        {
            return DataHelper::EnsureType($this->user_gets[$key], $dft);
        }
        return $dft;
    }

    /**
     * send data to the client
     * @param array $data
     * @return bool If success return True, fail return False
     */
    public function send(array $data)
    {
        return $this->server->push($this->fd, json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    /**
     * send data to the client
     * @param string $data
     * @return bool If success return True, fail return False
     */
    public function sendText(string $data)
    {
        return $this->server->push($this->fd, $data);
    }
}
