<?php
namespace Swork\Server\Ws;

use Swork\Bean\Holder\ControllerHolder;
use Swork\Bean\Holder\InstanceHolder;
use Swork\Exception\ValidateException;
use Swork\Exception\WsException;
use Swork\Exception\WsValidateException;

/**
 * WebSocket接收处理器
 * Class WsReceive
 * @package Swork\Server\WebSocket
 */
class WsReceive
{
    /**
     * 全局环境变量
     * @var array
     */
    private $env;

    /**
     * Request constructor.
     * @param array $env
     */
    public function __construct(array $env)
    {
        $this->env = $env;
    }

    /**
     * websocket回调
     * @param WsArgument $argument
     * @return mixed
     * @throws
     */
    function handler(WsArgument $argument)
    {
        //获取Class
        $cls = ControllerHolder::getClass($argument->getUri(), $params);
        if ($cls == false)
        {
            throw new WsException('Can not find the router.', 9001);
        }
        if ($params != null)
        {
            $argument->setParams($params);
        }

        //实例化
        $ins = InstanceHolder::getClass($cls[0]);
        if ($ins == false)
        {
            throw new WsException('Can not find the class.', 9002);
        }

        //调用方法
        $method = $cls[1];

        //处理请求参数检验
        try
        {
            WsValidator::check($cls[0], $method, $argument);
        }
        catch (ValidateException $ex)
        {
            throw new WsValidateException($ex->getMessage(), $ex->getCode());
        }

        //执行调用并返回
        return $ins->$method($argument);
    }
}
