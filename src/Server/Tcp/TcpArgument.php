<?php
namespace Swork\Server\Tcp;

use Swork\Helper\TcpHelper;
use Swork\Server\ArgumentInterface;

/**
 * TCP传输的参数对象
 * Class TcpArgument
 * @package Swork\Server\Tcp
 */
class TcpArgument implements ArgumentInterface
{
    /**
     * server
     * @var \swoole_server
     */
    private $server;

    /**
     * 来源fd
     * @var int
     */
    private $fd;

    /**
     * 请求URI
     * @var string
     */
    private $uri;

    /**
     * 内容实体
     * @var string
     */
    private $body;

    /**
     * 用户简易路径的参数
     * @var array
     */
    private $params = [];

    /**
     * 是否停止往下运行
     * @var bool
     */
    private $break = false;

    /**
     * TcpArgument constructor.
     * @param \swoole_server $server
     * @param int $fd
     * @param string $uri
     */
    public function __construct(\swoole_server $server, int $fd, string $uri)
    {
        $this->server = $server;
        $this->fd = $fd;
        $this->uri = $uri;
    }

    /**
     * @return \swoole_server
     */
    public function getServer(): \swoole_server
    {
        return $this->server;
    }

    /**
     * 设置返回的Header
     * @param string $key 头KEY
     * @param string $value 具体内容
     */
    public function setHeader(string $key, string $value)
    {

    }

    /**
     * 设置截断
     */
    public function setBreak()
    {
        $this->break = true;
    }

    /**
     * 判断是否截断
     */
    public function isBreak()
    {
        return $this->break;
    }

    /**
     * 获取连接句柄
     * @return int
     */
    public function getFd()
    {
        return $this->fd;
    }

    /**
     * 获取路由地址
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * 设置内容
     * @param string $body
     */
    public function setBody(string $body)
    {
        $this->body = $body;
    }

    /**
     * 获取内容
     * @return string 内容实体
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * 设置参数
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * 获取客户端IP
     * @return string
     */
    public function getUserIP()
    {
        return $this->server->getClientInfo($this->fd)['remote_ip'] ?? '';
    }

    /**
     * 根据key取出内容值（无实现）
     * @param string|null $key 键
     * @param mixed $dft 默认值
     * @return mixed
     */
    public function get($key = null, $dft = null)
    {
        return $this->body;
    }

    /**
     * send data to the client
     * @param string $data
     * @return bool If success return True, fail return False
     */
    public function send(string $data)
    {
        return $this->server->send($this->fd, $data);
    }

    /**
     * 通过内部规定的格式发送内容
     * @param string $cmd 命令头
     * @param string $content 命令内容
     * @param int $server_socket
     * @return bool
     */
    public function sendByInnerFormat(string $cmd, string $content = '', int $server_socket = -1)
    {
        //组装发送的数据
        $rel = TcpHelper::assemblyContent($cmd, $content);

        //执行发送
        return $this->server->send($this->fd, $rel, $server_socket);
    }
}
