<?php
namespace Swork\Server\Tcp;

/**
 * TCP处理接口
 * Interface TcpHandlerInterface
 * @package Swork\Server\Tcp
 */
interface TcpHandlerInterface
{
    /**
     * 获取路径
     * @return string
     */
    public function getUri();

    /**
     * 获取内容
     * @return string
     */
    public function getBody();

    /**
     * 是否截断
     * @return bool
     */
    public function isBreak();

    /**
     * TCP Receive
     * @param \swoole_server $server
     * @param $fd
     * @param $from_id
     * @param $data
     */
    public function onReceive(\swoole_server $server, $fd, $from_id, $data);

    /**
     * 监听 onclose
     * @param \swoole_server $server
     * @param int $fd
     */
    public function onClose(\swoole_server $server, int $fd);
}
