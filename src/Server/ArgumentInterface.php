<?php
namespace Swork\Server;

/**
 * 请求参数的接口
 * Interface ArgumentInterface
 * @package Swork\Server
 */
interface ArgumentInterface
{
    /**
     * 获取GET值
     * @param string|null $key 为空全部返回
     * @param mixed $dft 默认值
     * @return mixed
     */
    public function get($key = null, $dft = null);

    /**
     * 设置返回的Header
     * @param string $key 头KEY
     * @param string $value 具体内容
     */
    public function setHeader(string $key, string $value);

    /**
     * 设置其它参数
     * @param array $values 参数值（键值对JSON）
     */
    public function setParams(array $values);

    /**
     * 设置截断
     */
    public function setBreak();

    /**
     * 判断是否截断
     */
    public function isBreak();

    /**
     * 获取请求URI
     * @return string
     */
    public function getUri();

    /**
     * 获取请求IP
     * @return string
     */
    public function getUserIP();
}
