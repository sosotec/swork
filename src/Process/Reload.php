<?php
namespace Swork\Process;

use Swork\Service;

class Reload
{
    private $pidFile = '';
    private $watchDir = '';
    private $interval = 3;

    public function __construct(array $env)
    {
        $this->pidFile = $env['runtime'] . 'swork.pid';
        $this->watchDir = $env['root'] . 'app';
    }

    /**
     * 初始化
     */
    public function init()
    {
        $process = new \swoole_process(function (\swoole_process $process) {
            $prevMd5 = self::md5File($this->watchDir);
            while (true)
            {
                sleep($this->interval);
                $currentMd5 = self::md5File($this->watchDir);
                if (strcmp($prevMd5, $currentMd5) !== 0)
                {
                    $this->killMaster();
                    Service::$server->reload();
                    $prevMd5 = $currentMd5;
                    echo date('Y-m-d H:i:s') . ' reloaded' . PHP_EOL;
                }
            }
        });
        Service::$server->addProcess($process);
    }

    /**
     * 关闭所有用户进程
     */
    private function closeUserProcesses()
    {
        UserProcess::closeToExit();
    }

    /**
     * 读取文件md5值
     * @param string $dir
     * @return bool|string
     */
    private function md5File($dir)
    {
        if (!is_dir($dir))
        {
            return '';
        }

        //递归读取文件
        $md5File = [];
        $d = dir($dir);
        while (false !== ($entry = $d->read()))
        {
            if ($entry !== '.' && $entry !== '..')
            {
                if (is_dir($dir . '/' . $entry))
                {
                    $md5File[] = self::md5File($dir . '/' . $entry);
                }
                elseif (substr($entry, -4) === '.php')
                {
                    $md5File[] = md5_file($dir . '/' . $entry);
                }
                $md5File[] = $entry;
            }
        }
        $d->close();

        //计算MD5
        return md5(implode('', $md5File));
    }

    /**
     * kill掉主进程
     */
    private function killMaster()
    {
        $arr = Service::$pidManager->readPidArray();
        if (isset($arr['master']))
        {
            posix_kill($arr['master'], 0);
        }
    }
}
