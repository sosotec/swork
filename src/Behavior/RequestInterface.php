<?php
namespace Swork\Behavior;

use Swork\Server\Http\Argument;

/**
 * Request请求的接口
 * RequestInterface ArgumentInterface
 * @package Swork\Behavior
 */
interface RequestInterface
{
    /**
     * 请求开始处理
     * @param string $uri 请求地址
     * @param Argument $argument
     * @return string|bool 请求的KEY，md5(uri + get + stime + rand)
     */
    public function begin(string $uri, Argument $argument);

    /**
     * @param string $key 请求的KEY，从begin中来
     * @param Argument $argument
     * @param mixed $result 处理结果
     * @return mixed
     */
    public function end(string $key, Argument $argument, $result);
}
