<?php
namespace Swork;

use Swork\Bean\Holder\BreakerHolder;
use Swork\Bean\Holder\ControllerHolder;
use Swork\Bean\Holder\ExceptionHandlerHolder;
use Swork\Bean\Holder\MiddlewareHolder;
use Swork\Bean\Holder\ServiceHolder;
use Swork\Bean\Holder\TimerTaskHolder;
use Swork\Bean\Holder\ProcessHolder;
use Swork\Bean\Holder\UsingHolder;
use Swork\Bean\Holder\ValidateHolder;
use Swork\Bean\Scanner;
use Swork\Pool\Amqp\AmqpConfig;
use Swork\Pool\Amqp\AmqpPool;
use Swork\Pool\ElasticSearch\ElasticSearchConfig;
use Swork\Pool\ElasticSearch\ElasticSearchPool;
use Swork\Pool\MySql\MySqlConfig;
use Swork\Pool\MySql\MySqlPool;
use Swork\Pool\PoolCollector;
use Swork\Pool\Redis\RedisConfig;
use Swork\Pool\Redis\RedisPool;
use Swork\Pool\Rpc\RpcConfig;
use Swork\Pool\Rpc\RpcPool;

class Initialize
{
    /**
     * @param int $workerId
     * @return Scanner
     */
    public function collect(int $workerId)
    {
        //扫描文件
        $scanner = new Scanner(Service::$env, $workerId);
        $scanner->collect();

        //保存至Holder中
        ExceptionHandlerHolder::setHolder($scanner->getExceptionHandler());
        MiddlewareHolder::setHolder($scanner->getMiddleware());
        ControllerHolder::setHolder($scanner->getController());
        ControllerHolder::setRegex($scanner->getUriMatch());
        ValidateHolder::setHolder($scanner->getValidate());
        ServiceHolder::setHolder($scanner->getService());
        ProcessHolder::setHolder($scanner->getProcess());
        BreakerHolder::setHolder($scanner->getBreaker());
        UsingHolder::setHolder($scanner->getUsing());
        TimerTaskHolder::setHolder($scanner->getTimerTask());

        //返回
        return $scanner;
    }

    /**
     * 初始化连接池容器
     */
    public function db()
    {
        foreach (Configer::get('db') as $key => $value)
        {
            $config = new MySqlConfig($value, $key);
            $pool = new MySqlPool($config);
            PoolCollector::collect(PoolCollector::MySQL, $key, $pool);
        }
    }

    /**
     * 初始化Redis连接池
     */
    public function redis()
    {
        $instance = 'default';
        $config = new RedisConfig(Configer::get('redis'), $instance);
        $pool = new RedisPool($config);
        PoolCollector::collect(PoolCollector::Redis, $instance, $pool);
    }

    /**
     * 初始化Rpc连接池
     */
    public function rpc()
    {
        foreach (Configer::get('rpc') as $key => $value)
        {
            $config = new RpcConfig($value, $key);
            $pool = new RpcPool($config);
            PoolCollector::collect(PoolCollector::Rpc, $key, $pool);
        }
    }

    /**
     * 初始化amqp连接池
     */
    public function amqp()
    {
        $instance = 'default';
        $config = new AmqpConfig(Configer::get('amqp'), $instance);
        if (!$config->isEnable())
        {
            return;
        }
        $pool = new AmqpPool($config);
        PoolCollector::collect(PoolCollector::Amqp, $instance, $pool);
    }

    /**
     * 初始化ElasticSearch连接池
     */
    public function ElasticSearch()
    {
        $instance = 'default';
        $config = new ElasticSearchConfig(Configer::get('esc'), $instance);
        if (!$config->isEnable())
        {
            return;
        }
        $pool = new ElasticSearchPool($config);
        PoolCollector::collect(PoolCollector::ElasticSearch, $instance, $pool);
    }
}
