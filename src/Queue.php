<?php
namespace Swork;

use Swork\Queue\QueueInterface;
use Swork\Queue\RedisQueue;

/**
 * 系统队列组件
 * Class Cache
 * @package Swork
 */
class Queue
{
    /**
     * 接口实例化
     * @var QueueInterface
     */
    private static $_instance = null;

    /**
     * 根据KEY压入队列
     * @param string $key 队列KEY
     * @param array $data 应用数据（array数组）
     * @param int $rtime 在消息没有确认消费后，自动恢复时间（单位：分钟）默认1分钟
     * @return mixed
     */
    public static function push(string $key, array $data, int $rtime = 1)
    {
        if (self::$_instance == null)
        {
            self::$_instance = new RedisQueue();
        }
        return self::$_instance->push($key, $data, $rtime);
    }

    /**
     * 根据KEY提取队列，返回数组[id:xxx, data:yyy]（xxx是消息ID，yyy是原始应用数据）
     * @param string $key 队列KEY
     * @return mixed
     */
    public static function pop(string $key)
    {
        if (self::$_instance == null)
        {
            self::$_instance = new RedisQueue();
        }
        return self::$_instance->pop($key);
    }

    /**
     * 确认消费队列完成
     * @param string $key 队列KEY
     * @param string $id 消息ID
     * @return mixed
     */
    public static function ack(string $key, string $id)
    {
        if (self::$_instance == null)
        {
            self::$_instance = new RedisQueue();
        }
        return self::$_instance->ack($key, $id);
    }
}


