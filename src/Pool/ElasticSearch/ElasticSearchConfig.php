<?php
namespace Swork\Pool\ElasticSearch;

use Swork\Pool\AbstractConfig;

class ElasticSearchConfig extends AbstractConfig
{
    /**
     * 获取超时时间
     * @return string
     */
    public function getTimeout()
    {
        return $this->confg['timeout'] ?? 0;
    }

    /**
     * 是否启用ES
     * @return string
     */
    public function isEnable()
    {
        return $this->confg['enable'] ?? false;
    }
}
