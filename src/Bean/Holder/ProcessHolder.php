<?php
namespace Swork\Bean\Holder;

/**
 * Process收集器
 * Class ProcessHolder
 * @package Swork\Bean\Holder
 */
class ProcessHolder
{
    /**
     * @var array
     */
    private static $holder = [];

    /**
     * 设置容器值
     * @param array $holder
     */
    public static function setHolder(array $holder)
    {
        foreach ($holder as $cls => $item)
        {
            self::$holder[$cls] = $item;
        }
    }
}
