<?php
namespace Swork\Queue;

/**
 * 队列接口
 * Interface QueueInterface
 * @package Swork\Queue
 */
interface QueueInterface
{
    /**
     * 根据KEY压入队列
     * @param string $key 队列KEY
     * @param array $data 应用数据
     * @param int $rtime 队列恢复时间（单位：分钟）
     * @return mixed
     */
    public function push(string $key, array $data, int $rtime = 1);

    /**
     * 根据KEY提取队列
     * @param string $key 队列KEY
     * @return mixed
     */
    public function pop(string $key);

    /**
     * 确认消费队列完成
     * @param string $key 队列KEY
     * @param string $id 消息ID
     */
    public function ack(string $key, string $id);
}
