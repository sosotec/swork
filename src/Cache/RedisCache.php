<?php
namespace Swork\Cache;

use Swork\Client\Redis;

/**
 * Redis实现的缓存
 * Class RedisCache
 * @package Swork\Cache
 */
class RedisCache implements CacheInterface
{
    /**
     * 收集器
     * @var Redis
     */
    private static $redis = null;

    /**
     * Redis入口
     * @return Redis
     */
    public function __construct()
    {
        if (self::$redis == null)
        {
            self::$redis = new Redis();
        }
        return self::$redis;
    }

    /**
     * 根据KEY删除缓存
     * @param string $key
     * @return mixed
     */
    public function del(string $key)
    {
        return self::$redis->del($key);
    }

    /**
     * 根据KEY获取缓存
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return self::$redis->get($key);
    }

    /**
     * 设置缓存内容
     * @param string $key
     * @param mixed $value 要缓存的内容
     * @param int $timeout 超时时间（单位秒）
     * @return mixed
     */
    public function set(string $key, $value, int $timeout = 0)
    {
        return self::$redis->set($key, $value, $timeout);
    }
}
