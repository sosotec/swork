<?php
namespace Swork\Cache;

/**
 * 缓存接口
 * Interface CacheInterface
 * @package Swork\Cache
 */
interface CacheInterface
{
    /**
     * 根据KEY删除缓存
     * @param string $key
     * @return mixed
     */
    public function del(string $key);

    /**
     * 根据KEY获取缓存
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * 设置缓存内容
     * @param string $key
     * @param mixed $value 要缓存的内容
     * @param int $timeout 超时时间（单位秒）
     * @return mixed
     */
    public function set(string $key, $value, int $timeout = 0);
}
