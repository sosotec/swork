<?php
namespace Swork;

/**
 * 软件版本
 * Class Version
 * @package Swork
 */
class Version
{
    /**
     * 获取Composer的swork版本
     * @param array $env
     * @return string
     */
    public static function getComposer(array $env)
    {
        $num = 'NULL';

        //获取Composer文件
        $file = $env['root'] . 'composer.json';
        if (file_exists($file))
        {
            $text = file_get_contents($file);
            if ($text != false)
            {
                $conf = json_decode($text, true);
                if ($conf != false)
                {
                    $num = $conf['require']['soso/swork'] ?? 'NULL';
                }
            }
        }

        //返回
        return $num;
    }
}


