# 读写分离

支持按整库读写分离，框架内部没有实现多读，可通过外部负载均衡（SLB）来实现。

- 一个连接池节点支持配置一个只读节点
- 在配置只读节点后，此连接池之下的所有 读取数据的操作 都会走只读节点数据库，不再走主节点
- SLB：如HaProxy，F5 等

> 注意： 暂时不支持多读节点。

## 使用方式

### 在config/db.php配置文件中修改配置文件

- 在默认配置增加 read 配置键，用它指向一个连接池名称

## 示例

```php

<?php
return [
    
    //默认主节点连接池
    'default' => [
        'uri' => [
            'host' => '127.0.0.1',
            'port' => 3306,
            'user' => 'root',
            'password' => '123456',
            'database' => 'test',
            'charset' => 'utf8',
        ],
        'read' => 'default_read',  //增加read配置键，值指向【只读节点连接池】
        'pools' => 50,
    ],
    
    //只读节点连接池（配置格式与其它一致）
    'default_read' => [
        'uri' => [
            'host' => '127.0.0.1',
            'port' => 3306,
            'user' => 'root',
            'password' => '123456',
            'database' => 'test_read',
            'charset' => 'utf8',
        ],
        'pools' => 50,
    ],
];


```