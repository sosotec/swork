# 强制索引

支持select、update两个操作索引走指定索引

- select * from T force index (idx1) where ....
- update T force index (idx1) set A=1 where ....

> 注意： 暂时不支持delete。

## 使用方式

### $index 关键词

- where条件中使用 $index 关键词，与 $or， $and 并行使用

- 数据类型为字符串，如 idx1 或 idx1,idx2

## 示例

```php

 /**
 * 通过条件更新
 * @param Argument $arg
 * @return mixed
 */
public function index1(Argument $arg)
{
    $data = [
        'tname' => 'username',
    ];
    $where = [
        'tid' => 1,
        '$index' => 'idx1'
    ];
    return $this->testModel->update($where, $data);
}

```