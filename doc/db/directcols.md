# 直接更新字段

除通过变量更新数值外，支持使用 $direct 参数直接更新字段内容

> update set A=B+1,C=1,E=? where D=1;


## 示例

```php

/**
 * 通过条件更新
 * @param Argument $arg
 * @return mixed
 */
public function update(Argument $arg)
{
    //原参数更新数值
    $data = [
        'tname' => 'username',
    ];
    
    //直接按字段更新
    $direct = [
        'utime' => 'atime',
        'hits' => 'hits + 1',
        'left' => 'left - 1',
        'total' => 'hits + left',
    ];
    return $this->testModel->update(['tid' => 1], $data, $direct);
}

```