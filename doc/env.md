# 多环境支持
 通过config.ini文件配置使用哪个环境的配置
 
 > env指定的名字与config目录之下的文件夹名称需要一致

```ini
[system]
env=product #（可任何定义名称，与 config 目录一致即可）

```

