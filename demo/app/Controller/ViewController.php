<?php
namespace Demo\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\View;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/view")
 */
class ViewController extends BeanCollector
{
    /**
     * 测试
     * @View(web/body.tpl)
     * @param Argument $arg
     * @return mixed
     * @throws
     */
    public function index(Argument $arg)
    {
        return [
            'title' => 'template 引擎',
            'web' => [
                'title' => 'web engine',
                'time' => time(),
                'num' => 9
            ],
            'list' => [
                [
                    'key' => 1,
                    'value' => '广东省',
                    'list' => [
                        [
                            'key' => 11,
                            'value' => '深圳市',
                            'list' => [
                                ['key' => 111, 'value' => '罗湖区', 'pops' => 1000],
                                ['key' => 112, 'value' => '福田区', 'pops' => 2000],
                                ['key' => 113, 'value' => '南山区', 'pops' => 3000],
                                ['key' => 114, 'value' => '宝安区', 'pops' => 4000],
                            ]
                        ],
                        [
                            'key' => 12,
                            'value' => '广州市',
                            'list' => [
                                ['key' => 121, 'value' => '海珠区', 'pops' => 1500],
                                ['key' => 122, 'value' => '天河区', 'pops' => 3000],
                            ]
                        ],
                    ]
                ],
                [
                    'key' => 2,
                    'value' => '海南省',
                    'list' => [
                        [
                            'key' => 21,
                            'value' => '海口市',
                            'list' => []
                        ],
                        [
                            'key' => 22,
                            'value' => '琼州市',
                            'list' => []
                        ],
                    ]
                ],
            ],
            'agn' => [
                'list' => [
                    ['key' => 'key1', 'value' => 'value1'],
                    ['key' => 'key2', 'value' => 'value2'],
                ]
            ],
            'txt' => ['text1', 'text2', 'text3'],
            'name' => 'template engine',
            'age' => '1 year',
        ];
    }
}
