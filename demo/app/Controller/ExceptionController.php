<?php
namespace Demo\App\Controller;

use Demo\App\Exception\AppException;
use Demo\App\Exception\SworkException;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/exception")
 */
class ExceptionController extends BeanCollector
{
    /**
     * @param Argument $arg
     * @throws
     */
    public function app(Argument $arg)
    {
        throw new AppException('App Exception', 101);
    }

    /**
     * @param Argument $arg
     * @throws
     */
    public function swork(Argument $arg)
    {
        throw new SworkException('Swork Exception', 102);
    }

    /**
     * @param Argument $arg
     * @throws
     */
    public function sys(Argument $arg)
    {
        throw new \Exception('System Exception', 103);
    }
}
