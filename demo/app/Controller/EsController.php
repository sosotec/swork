<?php
namespace Demo\App\Controller;

use Demo\App\Model\SearchModel;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Server\Http\Argument;

/**
 * ElasticSearch入口
 * @Controller("/es")
 */
class EsController extends BeanCollector
{
    public function t0(Argument $argument)
    {
        return SearchModel::M()->getList([], '*', 2, 2);
    }

    public function t1(Argument $argument)
    {
        $where = [
            'age' => ['<=' => 50],
            '$min_score' => 0.5
        ];
        return SearchModel::M()->getList($where);
    }

    public function t2(Argument $argument)
    {
        $where = [
            'desc' => '广东江西中通',
            '$min_score' => 2
        ];
        return SearchModel::M()->getList($where);
    }

    public function t3(Argument $argument)
    {
        $where = [
            'name' => '梁代法',
            'age' => ['>' => 20, '<' => 50]
        ];
        return SearchModel::M()->getList($where);
    }

    public function t4(Argument $argument)
    {
        $where = [
            'age' => ['>' => 20, '<' => 50],
            'sex' => ['term' => 1]
        ];
        return SearchModel::M()->getList($where);
    }

    public function t5(Argument $argument)
    {
        $where = [
            '' => '广东江西高州45',
            '$min_score' => 2
        ];
        return SearchModel::M()->getList($where);
    }

    public function insert(Argument $argument)
    {
        $data = [
            'name' => '梁代法',
            'desc' => '广东省高州市大井镇六祥荔枝村45号',
            'sex' => 1,
            'age' => 41,
            'atime' => date('Y-m-d H:i:s')
        ];
        return SearchModel::M()->insert($data);
    }

    public function inserts(Argument $argument)
    {
        $data = [
            [
                'name' => '张亚男1',
                'desc' => '河北省##张家口市##宣化区##人民医院东50米(宣府大街南)',
                'sex' => 2,
                'age' => 21,
                'atime' => date('Y-m-d H:i:s')
            ],
            [
                'name' => '钟小辉2',
                'desc' => '江西省##赣州市##于都县##小溪乡中通快递旁边',
                'sex' => 1,
                'age' => 25,
                'atime' => date('Y-m-d H:i:s')
            ]
        ];
        return SearchModel::M()->inserts($data);
    }

    public function update(Argument $argument)
    {
        $where = [
            'name' => '梁代法'
        ];
        $data = [
            'age' => 48,
            'atime' => date('Y-m-d H:i:s')
        ];
        return SearchModel::M()->update($where, $data);
    }

    public function update2(Argument $argument)
    {
        $data = [
            'name' => '吴国栋',
            'age' => 32,
            'desc' => '广东省##潮州市##湘桥区##育苗幼儿园(卫星二路东)',
            'atime' => date('Y-m-d H:i:s')
        ];
        return SearchModel::M()->updateById('CBORp3EBE2gP5K9hBbgb', $data);
    }

    public function delete2(Argument $argument)
    {
        return SearchModel::M()->deleteById('BxORp3EBE2gP5K9hALjj');
    }

    public function init(Argument $argument)
    {
        return SearchModel::M()->init();
    }
}
