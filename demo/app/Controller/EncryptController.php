<?php
namespace Demo\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\BeanCollector;
use Swork\Helper\EncryptHelper;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/encrypt")
 */
class EncryptController extends BeanCollector
{
    /**
     * 加密
     * @param Argument $arg
     * @return mixed
     * @throws
     */
    public function type1(Argument $arg)
    {
        $string = 'http://www.aa.com/aa.jtm?di=xxx33&dd=223';
        $key = '6vHfAndR2r0GWxaQJnx286tmJsAyo10L';
        $encode = EncryptHelper::Type1Encode($string, $key);
        $decode = EncryptHelper::Type1Decode($encode, $key);

        return [
            'string' => $string,
            'key' => $key,
            'encode' => $encode,
            'decode' => $decode,
        ];
    }
}
