<?php
namespace Demo\App\Controller;

use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\BeanCollector;
use Swork\Client\Redis;
use Swork\Server\Http\Argument;

/**
 * Class IndexController
 * @Controller("/redis")
 */
class RedisController extends BeanCollector
{
    /**
     * @Inject()
     * @var Redis
     */
    private $redis;

    public function index(Argument $arg)
    {
        $rel1 = $this->redis->set('test1', microtime(true));
        $rel2 = $this->redis->set('test2', microtime(true));
        $rel3 = $this->redis->set('test3', microtime(true));
        return [$rel1, $rel2, $rel3];
    }

    /**
     * 测试
     * @param Argument $args
     * @return mixed
     * @throws
     */
    public function get(Argument $args)
    {
        $rel1 = $this->redis->get('test1');
        $rel2 = $this->redis->get('test2');
        $rel3 = $this->redis->get('test3');
        return [$rel1, $rel2, $rel3];
    }
}
