<?php
namespace Demo\App\Controller;

use Demo\App\Breaker\HttpGetBreaker;
use Demo\App\Exception\AppException;
use Demo\App\Logic\TestLogic;
use Demo\App\Middleware\ClassAfterMiddleware;
use Demo\App\Middleware\ClassBeforeMiddleware;
use Demo\App\Middleware\MethodAfterMiddleware;
use Demo\App\Middleware\MethodBeforeMiddleware;
use Demo\App\Model\TestModel;
use Demo\App\Model\UserModel;
use Demo\App\Service\IdInterface;
use Demo\App\Service\TestInterface;
use Demo\App\Task\TestTask;
use MongoDB\BSON\ObjectId;
use Swork\Bean\Annotation\Breaker;
use Swork\Bean\Annotation\Controller;
use Swork\Bean\Annotation\Inject;
use Swork\Bean\Annotation\Middleware;
use Swork\Bean\Annotation\Reference;
use Swork\Bean\BeanCollector;
use Swork\Client\Http;
use Swork\Client\MySql;
use Swork\Configer;
use Swork\Db\Db;
use Swork\Exception\DbException;
use Swork\Helper\IdHelper;
use Swork\Server\Http\Argument;
use Swork\Server\Tasker;

/**
 * Class IndexController
 * @Controller("/http")
 */
class HttpController extends BeanCollector
{
    /**
     * @Breaker(HttpGetBreaker::class)
     * @return array
     * @throws
     */
    public function get()
    {
//        $http = new Http();
//        $rel = $http->get('http://www.baidu.com');
//        IdHelper::generate();
//        return [$rel];


        //连接配置
//        $dsn = "mysql:host=localhost;port=3306;dbname=test;charset=utf8";
//        $pdoOpts = [
//            \PDO::ATTR_PERSISTENT => true,
//        ];

        //连接服务器
//        $connection = new \PDO($dsn, 'root', '123456', $pdoOpts);
//        $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
//        $connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

//        $connection->query(`INSERT INTO user (age,name,sex,desc) VALUES (18,"fengwanqing",1,"laji")`);


        return null;
    }

}
