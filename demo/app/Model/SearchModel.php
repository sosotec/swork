<?php
namespace Demo\App\Model;

use Swork\Db\ElasticSearchModel;

class SearchModel extends ElasticSearchModel
{
    public function __construct()
    {
        $tbl = 'user';
        $cols = [
            'name' => ['s', 'ik_max_word'],
            'desc' => ['s', 'ik_max_word'],
            'sex' => ['i', ''],
            'age' => ['i'],
            'atime' => ['s']
        ];
        parent::__construct($tbl, $cols);
    }
}
