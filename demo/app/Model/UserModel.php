<?php
namespace Demo\App\Model;

use Swork\Db\MySqlModel;
use Swork\Db\MySqlMonitorInterface;

class UserModel extends MySqlModel implements MySqlMonitorInterface
{
    public function __construct()
    {
        $tbl = 'user';
        $key = ['id', MySqlModel::KeyID];
        $cols = [
            'id' => ['i', 0],
            'name' => ['s', ''],
            'desc' => ['s', ''],
            'sex' => ['i', 0],
            'age' => ['i', 0],
            'atime' => ['i', 0]
        ];
        $inc = '';
        parent::__construct($tbl, $key, $cols, $inc);
    }

    /**
     * 注册插入数据监听
     * @return array|bool
     */
    public function registerInsertMonitor()
    {
        return ['name'];
    }

    /**
     * 注册更新数据监听
     * @return array|bool
     */
    public function registerUpdateMonitor()
    {
        return false;
    }

    /**
     * 注册删除数据监听
     * @return array|bool
     */
    public function registerDeleteMonitor()
    {
        return true;
    }

    /**
     * 监听插入数据回调
     * @param mixed $keyId 监控的回传主键ID
     * @param array $data 监控的回传数据
     */
    public function insertMonitorCallback($keyId, array $data)
    {
        sleep(1);
        var_dump($keyId);
        var_dump($data);
    }

    /**
     * 监听更新数据回调
     * @param array $where 当时运行的数据条件
     * @param array $data 监控的回传数据
     */
    public function updateMonitorCallback(array $where, array $data)
    {
        sleep(1);
        var_dump($where);
        var_dump($data);
    }

    /**
     * 监听删除数据回调
     * @param array $where 当时运行的数据条件
     */
    public function deleteMonitorCallback(array $where)
    {
        var_dump($where);
    }
}
