<?php
namespace Demo\App\Model;

use Swork\Db\MySqlModel;

class TestModel extends MySqlModel
{
    public function __construct()
    {
        $tbl = 'test';
        $key = ['tid', MySqlModel::AutoKeyID];
        $cols = [
            'tid' => ['i', 0],
            'tname' => ['s', ''],
            'uid' => ['i', 0],
            'sid' => ['i', 0],
            'atime' => ['i', 0],
            'num' => ['d', 0],
        ];
        $node = '';
        parent::__construct($tbl, $key, $cols, $node);
    }
}
