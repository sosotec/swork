<?php
namespace Demo\App\Exception;

use Swork\Bean\Annotation\ExceptionHandler;
use Swork\Exception\ExceptionHandlerInterface;
use Swork\Exception\HttpValidateException;
use Swork\Server\ArgumentInterface;

/**
 * Class AppException
 * @ExceptionHandler(HttpValidateException::class)
 * @package Demo\App\Exception
 */
class HttpValidatorExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * 处理异常
     * @param ArgumentInterface $argument 当前请求
     * @param \Throwable $ex 异常内容
     * @return mixed
     */
    public function handler(ArgumentInterface $argument, \Throwable $ex)
    {
        return [
            'status' => $ex->getCode(),
            'msg' => $ex->getMessage(),
            'from' => 'HttpValidatorExceptionHandler'
        ];
    }
}
