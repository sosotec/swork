<?php
namespace Demo\App\Amqp;

use Swoole\Process;
use Swork\Bean\Annotation\QueueTask;
use Swork\Bean\BeanCollector;
use Swork\Queue\AmqpArgument;

class TestAmqp2 extends BeanCollector
{
    /**
     * @param AmqpArgument $argument
     * @QueueTask("send_sms2")
     * @return bool
     * @throws
     */
    function test01(AmqpArgument $argument)
    {
        $channel = $argument->getChannel();
        $message = $argument->getMessage();
        $body = $argument->getBody();

        //处理发送短信任务
        //sleep(1);
        echo 'TestAmqp2 -> test01 -> ' . $body . PHP_EOL;

        //发送成功
        var_dump('[x2] 发送成功！');
        var_dump($body);

        //成功应答
        return true;
    }
}
