<?php
namespace Demo\App\Behavior;

use Swork\Behavior\RecorderInterface;
use Swork\Service;

class SqlRecorder implements RecorderInterface
{
    /**
     * 数据记录器
     * @param float $stime 执行开始时间
     * @param float $etime 执行结束时间
     * @param mixed $data 需要处理的数据
     * @param \Throwable|null $throwable 异常信息
     * @return mixed
     */
    public function process(float $stime, float $etime, $data, $throwable = null)
    {

    }
}
