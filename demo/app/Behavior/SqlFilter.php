<?php
namespace Demo\App\Behavior;

use Swork\Context;
use Swork\Behavior\FilterInterface;

class SqlFilter implements FilterInterface
{
    public function process(string &$sql, array &$data)
    {
        $context = Context::get('context-key');
        $sql = "/* mycat:db=$context */ $sql";
    }
}
