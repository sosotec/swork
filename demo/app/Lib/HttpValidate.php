<?php
namespace Demo\App\Lib;

use Demo\App\Exception\AppException;
use Swork\Server\Http\Argument;

class HttpValidate
{
    /**
     * 方法外自定义校验器
     * @param Argument $argument
     * @param int $method 获取数据方式
     * @param string $message 默认错误信息
     * @throws AppException
     */
    public static function Rule1(Argument $argument, int $method, string $message)
    {
        throw new AppException('自定义错误');
    }
}
