<?php
return [
    'uri' => [
        'host' => '127.0.0.1',
        'port' => 5672,
        'user' => 'guest',
        'password' => 'guest'
    ],
    'pools' => 50,
    'enable' => true,
    'threads' => false,
    'pubretry' => 2,
    'consumers' => [
        \Demo\App\Amqp\TestAmqp::class => [
            'test01' => true,
            'test02' => false,
        ],
        \Demo\App\Amqp\TestAmqp2::class => [
            'test01' => false,
        ],
    ],
];
