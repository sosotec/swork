<?php
return [
    'PDAAuth' => '/pos/pda/auth',
    'PDAScan' => '/pos/pda/scan',
    'PDABeat' => '/pos/pda/beat',
    'LEDAuth' => '/pos/led/auth',
    'LEDBeat' => '/pos/led/beat',
];
