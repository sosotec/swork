<?php
return [
    'uri' => [
        'host' => '127.0.0.1',
        'port' => 9200,
    ],
    'enable' => true,
    'timeout' => 3,
    'pools' => 50,
];
